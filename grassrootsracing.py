import bs4
from bs4 import BeautifulSoup
import requests

url = 'https://www.strava.com/segments/7852067'
result = requests.get(url)

doc = BeautifulSoup(result.text, "lxml")

table = doc.find('table')

headers = []

for i in table.find_all('td'):
    times = i.text.strip()
    headers.append(times)

for row in table.find_all('tr')[1:]:
    data = row.find_all('td')
    row_data = [td.text.strip() for td in data]

    del row_data[0]
    del row_data[2]